package clases;

public class Jugador {
	private String nombre;
	private String apellidos;
	private int edad;
	private int dorsal;
	private String posicion;
	private int Ngol;
	public Jugador() {
		
	}
	public Jugador(String nombre, String apellidos, int edad, int dorsal, String posicion) {
		this.nombre = nombre;
		this.apellidos = apellidos;
		this.edad = edad;
		this.dorsal = dorsal;
		this.posicion = posicion;
	}
	public Jugador(String nombre, String apellidos, int edad, int dorsal, String posicion, int Ngol) {
		this(nombre, apellidos, edad, dorsal, posicion);
		this.Ngol=Ngol;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApellidos() {
		return apellidos;
	}
	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}
	public int getEdad() {
		return edad;
	}
	public void setEdad(int edad) {
		this.edad = edad;
	}
	public int getDorsal() {
		return dorsal;
	}
	public void setDorsal(int dorsal) {
		this.dorsal = dorsal;
	}
	public String getPosicion() {
		return posicion;
	}
	public void setPosicion(String posicion) {
		this.posicion = posicion;
	}
	public int getGol() {
		return Ngol;
	}
	public void setGol(int Ngol) {
		this.Ngol=Ngol;
	}
	@Override
	public String toString() {
		return "Datos del jugador: [nombre=" + nombre + ", apellidos=" + apellidos + ", edad=" + edad + ", dorsal=" + dorsal
				+ ", posicion=" + posicion + ", n�mero de goles=" + Ngol + "]";
	}
	
	
	
}

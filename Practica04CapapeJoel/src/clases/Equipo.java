package clases;

import java.util.Scanner;

public class Equipo {
	public static Scanner in = new Scanner(System.in);
	private String nombreDeEquipo;
	Jugador[] jugador;

	public Equipo() {

	}

	public Equipo(int maxJugadores) {
		this.jugador = new Jugador[maxJugadores];
	}

	public String getNombreDeEquipo() {
		return nombreDeEquipo;
	}

	public void setNombreDeEquipo(String nombreDeEquipo) {
		this.nombreDeEquipo = nombreDeEquipo;
	}

	public String introducirEquipo() {
		String nombreEquipo;
		nombreEquipo = in.nextLine();
		nombreDeEquipo = nombreEquipo;
		return nombreDeEquipo;
	}

	public void mostrarEquipo() {
		System.out.println(nombreDeEquipo);
	}

	// M�todo alta
	public void altaEquipo() {
		String nombre;
		String apellido;
		int edad;
		int dorsal;
		String posicion;
		int Ngol;
		for (int i = 0; i < jugador.length; i++) {
			if (jugador[i] == null) {
				jugador[i] = new Jugador();
				System.out.println("Dame el nombre del jugador");
				nombre = in.nextLine();
				System.out.println("Dame el apellido del jugador");
				apellido = in.nextLine();
				System.out.println("Dame la edad del jugador");
				edad = in.nextInt();
				System.out.println("Dame el dorsal que tiene");
				dorsal = in.nextInt();
				in.nextLine();
				System.out.println("Las posiciones en las que pueden jugar son: Portero, Cierre, Banda, Pivote");
				posicion = in.nextLine();
				System.out.println("Dame el n�mero de goles");
				Ngol = in.nextInt();
				in.nextLine();
				jugador[i].setNombre(nombre);
				jugador[i].setApellidos(apellido);
				jugador[i].setEdad(edad);
				jugador[i].setDorsal(dorsal);
				jugador[i].setPosicion(posicion);
				jugador[i].setGol(Ngol);
				break;

			}

		}
	}

	// 1.- introducimos el nombre del jugador que queremos buscar
	public String introducirNombre() {
		System.out.println("Dime el nombre del jugador");
		String nombre = in.nextLine();
		return nombre;
	}

	public Jugador buscarJugador(String nombre) {
		for (int i = 0; i < jugador.length; i++) {
			if (jugador[i] != null) {
				if (jugador[i].getNombre().equals(nombre)) {
					return jugador[i];
				}
			}
		}
		return null;
	}

	// metodo donde buscamos al Jugador
	// metodo donde volvemos a introducir el nombre del jugador que quieres eliminar
	public String borrarNombre() {
		System.out.println("Dime el nombre que quieres eliminar");
		String nombre = in.nextLine();
		return nombre;
	}

//metodo donde eliminas al jugador 
	public void borrarJugador(String nombre) {
		for (int i = 0; i < jugador.length; i++) {
			if (jugador[i] != null) {
				if (jugador[i].getNombre().equals(nombre)) {
					System.out.println("El jugador se ha eliminado correctamente");
					jugador[i] = null;
				}
			}
		}
	}

	// metodo listar jugadores general
	public void listarJugadores(String equipo1, String equipo2) {
		System.out.println("El " + equipo1 + " est� formado por:" + "\n");
		for (int i = 0; i <5; i++) {
			if (jugador[i] != null) {
				System.out.println(jugador[i]);
			}
		}
		System.out.println(" ");
		System.out.println("El " + equipo2 + " est� formado por:" + "\n");
		for (int i = 5; i <10; i++) {
			if (jugador[i] != null) {
				System.out.println(jugador[i]);
			}
		}
	}

	// metodo de cambiar informacion de alg�n jugador
	public String nombreDeJugador() {
		System.out.println("Dame el nombre del jugador que quieres cambiar la informaci�n");
		String nombreDeJugador = in.nextLine();
		return nombreDeJugador;
	}

	public void cambiarDatos(String nombreDeJugador) {
		System.out.println("Dame la nueva posicion del jugador");
		String posicion = in.nextLine();
		System.out.println("Dame la edad correcta del jugador");
		int edad = in.nextInt();
		for (int i = 0; i < jugador.length; i++) {
			if (jugador[i] != null) {
				if (jugador[i].getNombre().equals(nombreDeJugador)) {
					jugador[i].setPosicion(posicion);
					jugador[i].setEdad(edad);
					System.out.println("Datos actualizados correctamente");
				}
			}
		}
	}

	// MEN�
	public int menu() {
		System.out.println("Selecciona una opci�n");
		int n = in.nextInt();
		in.nextLine();
		return n;
	}

	// metodo de partido entre dos equipos
	public void resultadoPartido(String equipo1, String equipo2) {
		int golesLocal = (int) (Math.random() * 15);
		int golesVisitante = (int) (Math.random() * 15);
		if (golesLocal > golesVisitante) {
			System.out.println(equipo1 + " " + golesLocal + " - " + equipo2 + " " + golesVisitante);
		} else {
			System.out.println(equipo2 + " " + golesVisitante + " - " + equipo1 + " " + golesLocal);
		}
	}

	// metodo de lesion
	public void hayLesiones() {

		int lesion = (int) (Math.random() * 2);
		int jugadorLesionado[] = new int[4];
		for (int i = 0; i < jugadorLesionado.length; i++) {
			jugadorLesionado[i] = (int) (Math.random() * 8);
		}
		String nombres[] = new String[10];
		if (lesion == 1) {
			System.out.println("Ha habido lesiones en el partido");
			// Relleno el array nombres
			for (int i = 0; i < jugador.length; i++) {
				for (int j = i; j < nombres.length; j++) {
					if (jugador[i] != null) {
						nombres[j] = jugador[i].getNombre();
						break;
					}
				}
			}
			for (int i = 0; i < nombres.length; i++) {
				for (int j = 0; j < jugadorLesionado.length; j++) {
					if (i == jugadorLesionado[j]) {
						System.out.println("El jugador " + nombres[i] + " se ha lesionado");
						break;
					}
				}
			}
		} else {
			System.out.println("No ha habido lesiones en el partido");
		}
	}

	public void jugadorEnRacha() {
		String nombres[] = new String[10];
		int goles[] = new int[10];
		int mayor = 0;
		int posicion = 0;
		String nom = "";
		// Relleno array nombres
		for (int i = 0; i < jugador.length; i++) {
			for (int j = i; j < nombres.length; j++) {
				if (jugador[i] != null) {
					nombres[j] = jugador[i].getNombre();
					break;
				}
			}
		}

		// relleno array goles
		for (int i = 0; i < jugador.length; i++) {
			for (int j = i; j < goles.length; j++) {
				if (jugador[i] != null) {
					goles[j] = jugador[i].getGol();
					break;
				}
			}
		}
		// metodo del m�ximo

		for (int i = 0; i < goles.length; i++) {
			if (goles[i] > mayor) {
				mayor = goles[i];
				posicion = i;
			}
		}
		for (int i = 0; i < nombres.length; i++) {
			if (i == posicion) {
				nom = nombres[i];
			}
		}
		System.out.println("El jugador " + nom + " es el que est� en racha con " + mayor + " goles");
	}

// metodo listar jugadores por parametro
	public String buscarJugador() {
		System.out.println("Dime la posicion que quieres listar");
		String posicionListar = in.nextLine();
		return posicionListar;
	}

	public void listarJugadoresPorPosicion(String posicionListar) {
		for (int i = 0; i < jugador.length; i++) {
			if (jugador[i] != null) {
				if (jugador[i].getPosicion().equals(posicionListar)) {
					System.out.println(jugador[i]);
				}
			}
		}
	}
}

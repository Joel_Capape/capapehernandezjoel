package principal;

import clases.Equipo;

public class Principal {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int maxJugadores;
		maxJugadores=10;
		Equipo club= new Equipo();	
		Equipo jugadores= new Equipo(maxJugadores);
		Equipo partido = new Equipo();
		System.out.println("Dame el nombre del equipo local");
		String equipo1=club.introducirEquipo();
		System.out.println("Introduce los miembros del "+equipo1+ "\n");
		jugadores.altaEquipo();
		jugadores.altaEquipo();
		jugadores.altaEquipo();
		jugadores.altaEquipo();
		jugadores.altaEquipo();
		System.out.println("Introduce el nombre del equipo visitante");
		String equipo2=club.introducirEquipo();
		System.out.println("Introduce los miembros del "+equipo2+"\n");
		jugadores.altaEquipo();
		jugadores.altaEquipo();
		jugadores.altaEquipo();
		jugadores.altaEquipo();
		jugadores.altaEquipo();
		//MENU//
		int opcion;
		do {
			System.out.println("____________________________________________________");
			System.out.println("                        Men�                        ");
			System.out.println("1.-Buscar jugador");
			System.out.println("2.- Borrar jugador");
			System.out.println("3.- Listar todos los jugadores");
			System.out.println("4.- Cambiar datos de alg�n jugador");
			System.out.println("5.-Estad�sticas futbol�sticas");
			System.out.println("6.- Listar jugador por alg�n parametro");
			System.out.println("7.- Salir de la aplicaci�n");
			opcion=club.menu();
			System.out.println("____________________________________________________");
			switch (opcion) {
			case 1:
				String jugadorNombre=jugadores.introducirNombre();
				System.out.println(jugadores.buscarJugador(jugadorNombre));
				break;
			case 2:
				String nombre=jugadores.borrarNombre();
				jugadores.borrarJugador(nombre);
				break;
			case 3:
				jugadores.listarJugadores(equipo1, equipo2);
				break;
			case 4:
				String informacion=jugadores.nombreDeJugador();
				jugadores.cambiarDatos(informacion);
				break;
			case 5:
				int datos;
				do {
					System.out.println("____________________________________________________");
					System.out.println("             Datos Futbol�sticos                    ");
					System.out.println("1.- Resultado del partido");
					System.out.println("2.- Jugadores Lesionados");
					System.out.println("3.- Jugador en racha");
					System.out.println("4.- Volver al men� principal");
					System.out.println("____________________________________________________");
					datos=club.menu();
					switch(datos) {
					case 1:
						partido.resultadoPartido(equipo1, equipo2);
						break;
					case 2:
						jugadores.hayLesiones();
						break;
					case 3:
						jugadores.jugadorEnRacha();
						break;
					case 4:
						break;
						default:
							System.out.println("Opci�n no encontrada");
					}
				}while(datos!=4);
				break;
			case 6:
				String posicionListar=jugadores.buscarJugador();
				jugadores.listarJugadoresPorPosicion(posicionListar);
				break;
			case 7:
				System.out.println("La aplicaci�n se ha cerrado correctamente");
				System.exit(0);
				break;
				default:
					System.out.println("Opci�n no contemplada");
			}
		}while(opcion!=7);
				
	}

}
